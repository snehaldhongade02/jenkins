pipeline {
    agent any
    stages {
        stage('pull') { 
            steps {
               echo "successfull pull from private repo"
              git branch: 'main', url: 'https://gitlab.com/snehaldhongade02/jenkins.git'
            }
        }
        stage('build') { 
            steps {
                echo "build by maven " 
                sh '''
                /bin/apache-maven-3.9.5/bin/mvn clean package
                   '''
            }
        }
        stage('test') {
            steps {
                echo "test is getting successfull by sonarqube"
                withSonarQubeEnv(installationName: 'sonar' , credentialsId: 'sonar') {
                sh '/bin/apache-maven-3.9.5/bin/mvn sonar:sonar -Dsonar.projectKey=new-project'           
                }
             }
        }
        stage('deploy') {
            }
        }

        }
}  